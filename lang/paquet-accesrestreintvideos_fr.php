<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accesrestreintvideos_description' => 'Ce plugin fournit un modèle qui permet de restreindre l\'affichage de médias suivant les droits des visiteurs à des zones du plugin \"Accès restreint\".

Il est ainsi possible d\'insérer une vidéo restreinte à l\'intérieur d\'un article qui lui est public.',
	'accesrestreintvideos_nom' => 'Accès restreint aux vidéos',
	'accesrestreintvideos_slogan' => 'Restreindre le visionnage de médias',
);

?>