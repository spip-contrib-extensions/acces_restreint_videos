<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accesrestreintvideos_titre' => 'Accès restreint aux vidéos',
	
	// C
	'configurer_id_zone_defaut_explication' => 'La zone qui sera utilisée par défaut pour restreindre l’accès.',
	'configurer_id_zone_defaut_label' => 'Zone par défaut',
	'configurer_texte_restreint_explication' => 'Ce qui sera affiché au visiteur s’il n’a pas accès au contenu.',
	'configurer_texte_restreint_label' => 'Texte de la restriction',
	
	// T
	'texte_restreint' => 'Un média restreint est inséré ici, mais vous n’avez pas les droits pour le visionner.',
	'texte_restreint_titre' => 'Contenu restreint',
	'titre_page_configurer_accesrestreintvideos' => 'Configurer l\'accès restreint aux vidéos',
);

?>
